package swgui;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class RadioButtonsPanel extends JPanel {
    
    private ButtonGroup group;
    private JRadioButton button_50;
    private JRadioButton button_100;
    private JRadioButton button_150;
    private JRadioButton button_200;   
    public StartPanel startPanel;
    
    public RadioButtonsPanel(StartPanel startPanel) {
        this.startPanel = startPanel;
        RadioHandler handler = new RadioHandler();
        
        button_50 = new JRadioButton("50", true);
        button_50.addItemListener(handler);
        
        button_100 = new JRadioButton("100", false);
        button_100.addItemListener(handler);
        
        button_150 = new JRadioButton("150", false);
        button_150.addItemListener(handler);
        
        button_200 = new JRadioButton("200", false);
        button_200.addItemListener(handler);
        
        add(button_50);
        add(button_100);
        add(button_150);
        add(button_200);
        
        group = new ButtonGroup();
        group.add(button_50);
        group.add(button_100);
        group.add(button_150);
        group.add(button_200);
    }
    
    private class RadioHandler implements ItemListener {
        
        @Override
        public void itemStateChanged(ItemEvent ie) {
            if ( ie.getSource() == button_50 ) {
                startPanel.setLength(50);
            }
            else if ( ie.getSource() == button_100 ) {
                startPanel.setLength(100);
            }
            else if ( ie.getSource() == button_150 ) {
                startPanel.setLength(150);
            }
            else if ( ie.getSource() == button_200 ) {
                startPanel.setLength(200);
            }
        }
    }
}
